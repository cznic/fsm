fsm
===

Package fsm provides utilities for messing with finite state machines.

Installation

    $ go get modernc.org/fsm

Documentation: [godoc.org/modernc.org/fsm](http://godoc.org/modernc.org/fsm)
